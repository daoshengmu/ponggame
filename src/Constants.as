package
{
    public class Constants
    {
        private static var _gameWidth:int  = 400;
        private static var _gameHeight:int = 300;
        
        private static var _centerX:int = _gameWidth / 2;
        private static var _centerY:int = _gameHeight / 2;
		
		public static function set gameWidth( width:uint ):void 
		{
			_gameWidth = width;
			_centerX = _gameWidth / 2;
		}
		
		public static function set gameHeight( height:uint ):void 
		{
			_gameHeight = height;
			_centerY = _gameHeight / 2;
		}
		
		public static function get gameWidth():uint
		{
			return _gameWidth;
		}
		
		public static function get gameHeight():uint
		{
			return _gameHeight;
		}
		
		public static function get centerX():int
		{
			return _centerX;
		}
		
		public static function get centerY():int
		{
			return _centerY;
		}
    }
}