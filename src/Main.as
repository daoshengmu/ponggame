package 
{
	import flash.display.Sprite;
	import flash.display.StageScaleMode;
	import flash.display.StageAlign;
	import flash.system.Capabilities;
	
	import starling.events.KeyboardEvent;	
	import starling.core.Starling;
	import starling.utils.AssetManager;
	import starling.events.Event;
	import starling.textures.Texture;
	
	import mu3d.stage3d.Renderer;

	
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	
	 
	[SWF(width="1000", height="600", frameRate="60")]
	public class Main extends Sprite
	{
		[Embed(source = "/startup.jpg")]
        private var Background:Class;
		
		private var _render	:Renderer = null;
		private var _starling :Starling;
		
		
		public function Main()
		{
			Constants.gameWidth = stage.stageWidth;
			Constants.gameHeight = stage.stageHeight;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;								
			
			_start();
		}
		
		private function _start():void
		{
			stage.color = 0x000000;
			Starling.handleLostContext = true;
			_starling = new Starling( Game, stage, null, null, "auto", "baseline");
			_starling.enableErrorChecking = Capabilities.isDebugger;
			
			_starling.start();
			_starling.addEventListener( Event.ROOT_CREATED, _onRootCreated );			
			
		/*	_render = new Renderer();
			stage.addEventListener( Event.RESIZE, _onResize );
			stage.addEventListener( Event.ENTER_FRAME, _onEnterFrame );
			
			_render.initialize( stage, _contextCreated );*/
		}		
				
		private function _onRootCreated( event:starling.events.Event, game:Game ):void
		{
			if ( _starling.context.driverInfo.toLowerCase().indexOf("software") != 01 )
				_starling.nativeStage.frameRate = 30;
			
				
			var assets:AssetManager = new AssetManager();
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue( EmbeddedAssets );
			
		//	var bgTexture:starling.textures.Texture = Texture.fromEmbeddedAsset( Background, false );
			
			game.start( assets, stage.stageWidth, stage.stageHeight );
		}
		
	/*	private function _contextCreated( event:Event ):void
		{
			_render.configureBackBuffer( stage.stageWidth, stage.stageHeight, 0, true );
		}
		
		private function _onEnterFrame( event:Event ):void
		{
			if ( !_render )
				return;
			
			_render.clear( 1, 0, 0, 1 );
			_render.update();
			_render.draw();
		}
		
		private function _onResize( event:Event = null ):void
		{
			var i:uint = 0;
			++i;	
			
			
		}*/
		
	}
	
}