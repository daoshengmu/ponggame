package
{
    import flash.utils.getQualifiedClassName;    
    
	import scenes.GameScene;
	import scenes.AboutScene;
    
    import starling.core.Starling;
    import starling.display.Button;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    import starling.text.TextField;
    import starling.textures.Texture;
    import starling.utils.VAlign;

    public class MainMenu extends Sprite
    {
        public function MainMenu()
        {
            init();
        }
        
        private function init():void
        {
            var title:TextField = new TextField( 500, 250, "Pong Game", "Verdana", 64, 0xffffff );
			title.x = Constants.centerX - title.width * .5;
			title.y = Constants.gameHeight * 0.25;
            addChild(title);
            
            var scenesToCreate:Array = [
                ["1 PLAYER GAME", GameScene],
				["ABOUT GAME", AboutScene]               
            ];
            
            var buttonTexture:Texture = Game.assets.getTexture("button_medium");
            var count:int = 0;
            
            for each (var sceneToCreate:Array in scenesToCreate)
            {
                var sceneTitle:String = sceneToCreate[0];
                var sceneClass:Class  = sceneToCreate[1];
                
                var button:Button = new Button(buttonTexture, sceneTitle);
                button.x = Constants.centerX - 67;
                button.y = Constants.centerY + (count * 67) + 30;
                button.name = getQualifiedClassName(sceneClass);
                addChild(button);
                
                if (scenesToCreate.length % 2 != 0 && count % 2 == 1)
                    button.y += 24;
                
                ++count;
            }            
          
        }
        
    }
}