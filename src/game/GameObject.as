package game 
{
	import starling.display.Quad;
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class GameObject 
	{
		protected var _moveSpeed:int;
		protected var _gameObject:Quad;
		protected var _bUpdate:Boolean = false;
		protected var _moveDir :Vector2D = new Vector2D();
		protected var _halfWidth:Number = 0;
		protected var _halfHeight:Number = 0;
						
		private var _centerPoint:Vector2D = new Vector2D();
		private var _minPoint:Vector2D = new Vector2D();
		private var _maxPoint:Vector2D = new Vector2D();		
		
		public function GameObject() 
		{
			
		}
				
		public function set moveDir( v:Vector2D ):void 
		{
			_moveDir.value = v;
			_bUpdate = true;
		}
		
		public function get moveDir():Vector2D
		{
			return _moveDir;
		}		
		
		public function get moveSpeed():int
		{
			return _moveSpeed;
		}
				
		public function update():void
		{
			if ( _bUpdate )
			{				
				_gameObject.x = _centerPoint.x - _halfWidth;
				_gameObject.y = _centerPoint.y - _halfHeight;
				_minPoint.setByValue( _gameObject.x, _gameObject.y );
				_maxPoint.setByValue( _centerPoint.x + _halfWidth, _centerPoint.y + _halfHeight );
				
				_bUpdate = false;
			}
			
		}		
		
		public function get maxPoint():Vector2D
		{
			return _maxPoint;
		}
		
		public function get minPoint():Vector2D
		{
			return _minPoint;
		}
		
		public function set x( v:Number ):void
		{
			_centerPoint.x = v;
			_bUpdate = true;
		}
		
		public function get x():Number
		{
			return _centerPoint.x;
		}
		
		public function set y( v:Number ):void
		{
			_centerPoint.y = v;
			_bUpdate = true;
		}
		
		public function get y():Number
		{
			return _centerPoint.y;
		}
		
		
	}

}