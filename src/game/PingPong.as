package game 
{
	import scenes.Scene;
	import starling.display.Quad;
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class PingPong extends GameObject
	{		
		public static const PONG_SIZE:int = 10;
		public static const HALF_PONG_SIZE:int = PONG_SIZE * .5;		
		private var _bActive:Boolean = false;
		
		public function PingPong( scene:Scene ) 
		{			
			_moveSpeed = 10;
			_gameObject = new Quad( PONG_SIZE, PONG_SIZE, 0xffffff );
			_halfWidth = _halfHeight = PONG_SIZE*0.5;
			scene.addChild( _gameObject );
		}		
		
		public function set active( b:Boolean ):void
		{
			_bActive = b;
		}
		
		public function get active():Boolean
		{
			return _bActive;
		}
		
		public override function update():void
		{				
			this.x += _moveDir.x * _moveSpeed;
			this.y += _moveDir.y * _moveSpeed;
			
			super.update();		
		}
	}

}