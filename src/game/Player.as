package game 
{
	import scenes.Scene;
	import starling.display.Quad;
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class Player extends GameObject
	{
		public static const PLAYER_WIDTH :int = 20;
		public static const PLAYER_HEIGHT :int = 80;
		public static const HALF_PLAYER_WIDTH :int = PLAYER_WIDTH * .5;	
		public static const HALF_PLAYER_HEIGHT :int = PLAYER_HEIGHT * .5;	
		private static const HIT_THRESHOLD :int = 10;		
		
		public function Player( scene:Scene ) 
		{
			_moveSpeed = 20;
			 _gameObject =  new Quad( PLAYER_WIDTH, PLAYER_HEIGHT, 0xffffff );
			 _halfWidth = PLAYER_WIDTH * .5;
			 _halfHeight = PLAYER_HEIGHT * .5;
			scene.addChild( _gameObject );
		}
		
		public function hitThePong( pong:PingPong ):Boolean
		{
			// Hit by the player's left side
			if ( ( pong.moveDir.x > 0 )
				&& ( pong.minPoint.y >= this.minPoint.y ) 
				&& ( pong.maxPoint.y <= this.maxPoint.y )  				
				&& ( ( pong.maxPoint.x >= (this.minPoint.x - HIT_THRESHOLD) )
					&& ( pong.maxPoint.x <= (this.minPoint.x + HIT_THRESHOLD) )
					)				
				)				
				return true;
				
			// Hit by the player's right side	
			else if ( ( pong.moveDir.x < 0 )
				&& ( pong.maxPoint.y >=  this.minPoint.y ) 
				&& ( pong.minPoint.y <= this.maxPoint.y )
				&& ( ( pong.minPoint.x <= (this.maxPoint.x + HIT_THRESHOLD) )
					&& ( pong.minPoint.x >= (this.maxPoint.x - HIT_THRESHOLD) )
					)	
				)
				return true;
			
			return false;
		}
				
	}

}