package game 
{
	import scenes.Scene;
	import starling.display.Sprite;
	import starling.text.TextField;
	
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class ScoreBoard
	{
		private const _scoreBoardWidth:int = 120;
		private const _scoreBoardHeight:int = 100;
		
		private var _scoreA:TextField = new TextField( _scoreBoardWidth, _scoreBoardHeight, "0", "Verdana", 80, 0xffffff, true );
		private var _scoreB:TextField = new TextField( _scoreBoardWidth, _scoreBoardHeight, "0", "Verdana", 80, 0xffffff, true );
		
		
		public function ScoreBoard( scene:Scene ) 
		{
			_scoreA.x = Constants.centerX * 0.5 - (_scoreBoardWidth * .5);
			_scoreA.y = 30;
			
			scene.addChild( _scoreA );
			
			_scoreB.x = Constants.centerX + (Constants.centerX * .5) - (_scoreBoardWidth * .5);
			_scoreB.y = 30;
			
			scene.addChild( _scoreB );
		}
		
		public function set scoreA( num:int ):void
		{
			_scoreA.text = num.toString();
		}
		
		public function get scoreA():int
		{
			return int(_scoreA.text);
		}
		
		public function set scoreB( num:int ):void
		{
			_scoreB.text = num.toString();
		}
		
		public function get scoreB():int
		{
			return int(_scoreB.text);
		}
		
	}

}