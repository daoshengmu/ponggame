package game 
{
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class StringTest 
	{
		private var _s1:String = new String("This is a test string to say Hello World!");
		
		public function StringTest() 
		{
			//var reversedString:String = _s1.split("").reverse().join("");
			//
			//var lens:uint = _s1.length-1;
			//var i:uint = 0;
			//var c1:String, c2:String;
			//
			//while ( lens )
			//{				
				//_s1.replace( /t/, "$2-$0" );
				//
				//_s1.charAt( lens ) = _s1.charAt(i);
				//
				//--lens;
				//++i;
			//}
			
			var reverseStr:String = "";
			var lens:uint = _s1.length - 1;
			for ( var i:int = lens; i >= 0;  i-- )
			{
				reverseStr += _s1.substr( i, 1 );
			}
			
			trace( "reverse string: " + reverseStr );
		}
		
	}

}