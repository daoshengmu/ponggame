package game 
{
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class Vector2D 
	{
		public var x: Number = 0;
		public var y: Number = 0;
		
		
		public function Vector2D( vX:Number = 0, vY:Number = 0 ) 
		{
			x = vX;
			y = vY;
		}
		
		public function normalize():void
		{
			var sqrt:Number = Math.sqrt( x * x + y * y );
			
			x /= sqrt;
			y /= sqrt;
		}
		
		public function set value( v:Vector2D ):void
		{
			x = v.x;
			y = v.y;
		}
		
		public function setByValue( vX:Number, vY:Number ):void
		{
			x = vX;
			y = vY;
		}
		
	}

}