package geometry 
{
	import material.IMaterial;
	import material.MaterialType;
	
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class Cube
	{
		private var _geometry = new Geometry();
		private var _material:IMaterial = null;
		
		public function Cube() 
		{
			
		}
		
		public function CreateByMaterial( type:uint ):void
		{
			switch( type )
			{
				case MaterialType.COLOR:
					
					break;
					
				default:
					trace( "not support this type of material" );
					assert( 0 );
					break;					
			}
		}
		
	}

}