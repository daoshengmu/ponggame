package mu3d.stage3d 
{
	import flash.display.Stage;
	import flash.display.Stage3D;
	import flash.display3D.Context3D;
	import flash.display3D.Context3DProfile;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class Renderer 
	{
		private var _context: Context3D;
		private var _enableErrorChecking	: Boolean = true;
		private var _stage3dIdx: uint = 0;
		private var _stage3d:Stage3D = null;
		private var _context3DProfile:String = Context3DProfile.BASELINE;
		
		public function Renderer() 
		{
			
		}
		
		public function initialize( stage:Stage, contextCreatedCallback:Function ):void 
		{
			_stage3d = stage.stage3Ds[ _stage3dIdx ];
			_stage3d.addEventListener( Event.CONTEXT3D_CREATE, _context3dCreatedHandler );
			_stage3d.addEventListener( Event.CONTEXT3D_CREATE, contextCreatedCallback );
			
			_stage3d.requestContext3D( "auto", _context3DProfile );
		}
		
		public function configureBackBuffer( width:int, height:int, antiAlias:int, enableDepthAndStencil: Boolean ):void
		{
			_context.configureBackBuffer( width, height, antiAlias, enableDepthAndStencil );
		}
		
		public function clear( red:Number = 0.0, green:Number = 0.0, blue:Number = 0.0, alpha:Number = 1.0
										, depth:Number = 1.0, stencil:uint = 0, mask:uint = 0xffffffff ):void
		{
			_context.clear( red, green, blue, alpha, depth, stencil, mask );
		}
		
		public function update():void
		{			
			
		}
		
		public function draw():void
		{		
			_context.present();
		}		
		
		public function terminate():void
		{			
			_context.dispose();
		}
		
		private function _context3dCreatedHandler( event:Event ):void
		{			
			_context = _stage3d.context3D;
		}
		
		private function _drawTriangles( ):void
		{
			
		}
		
	}

}