package mu3d.stage3d 
{
	import flash.display3D.VertexBuffer3D;
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class VertexBuffer 
	{
		private var _vertexBuffer :VertexBuffer3D = null;
		
		public function VertexBuffer() 
		{
			
		}
		
		public function dispose():void
		{
			_vertexBuffer.dispose();
		}
		
	}

}