package scenes 
{
	import starling.core.Starling;
	import starling.text.TextField;
	import starling.utils.VAlign;
	import starling.events.TouchEvent;
    import starling.events.TouchPhase;
	
	/**
	 * ...
	 * @author Daosheng Mu
	 */
	public class AboutScene extends Scene 
	{
		
		public function AboutScene() 
		{
			super();
			
			  // show information about rendering method (hardware/software)
			var author:TextField = new TextField(310, 64, "Daosheng Mu", "Verdana", 32, 0xffffff );
            author.x = Constants.centerX - author.width * .5;
            author.y = 100;
			addChildAt(author, 0);
			
			var date:TextField = new TextField(310, 64, "2014.3.26", "Verdana", 32, 0xffffff );
            date.x = Constants.centerX - date.width * .5;
            date.y = 200;
			addChildAt(date, 0);
			
			var player1:TextField = new TextField(800, 80, "Player 1-\nmove up: W key\n move down: S key", "Verdana", 20, 0xffffff );
            player1.x = Constants.centerX - player1.width * .5;
            player1.y = 300;
			addChildAt(player1, 0);
			
			var player2:TextField = new TextField(800, 80, "Player 2-\nmove up: UP\n move down: DOWN key", "Verdana", 20, 0xffffff );
            player2.x = Constants.centerX - player2.width * .5;
            player2.y = 400;
			addChildAt(player2, 0);
			 
            var driverInfo:String = Starling.context.driverInfo;
            var infoText:TextField = new TextField(310, 64, driverInfo, "Verdana", 10, 0xffffff );
            infoText.x = Constants.centerX;
            infoText.y = 500;
            infoText.vAlign = VAlign.BOTTOM;
            infoText.addEventListener(TouchEvent.TOUCH, onInfoTextTouched);
            addChildAt(infoText, 0);
		}		
		
        private function onInfoTextTouched(event:TouchEvent):void
        {
            if (event.getTouch(this, TouchPhase.ENDED))
                Starling.current.showStats = !Starling.current.showStats;
        }
	}

}