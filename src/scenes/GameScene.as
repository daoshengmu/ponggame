package scenes 
{
	/**
	 * ...
	 * @author Daosheng Mu
	 */	
	
	import flash.utils.Dictionary;
	import game.PingPong;
	import game.Player;
	import game.ScoreBoard;
	import game.KeyCode;	
	import game.Vector2D;
	import starling.display.Image;
	import starling.display.Button;
	import starling.display.Quad;
    import starling.display.Sprite;
	import starling.textures.Texture;
	import starling.text.TextField;
	import starling.events.KeyboardEvent;
	import starling.events.Event;
    
	public class GameScene extends Scene
	{		
		private const _emitAngle: Number = 0.3;
		
		private var _backButton	:Button; 
		private var _offset :int = 40;
		
		private var _unitLine: Quad = new Quad( 10, 20, 0xffffff );
		
		private var _scoreBoard: ScoreBoard = null;
		
		private var _pingPong: PingPong = null;
		private var _playerA: Player = null;
		private var _playerB: Player = null;
		
		private var _keyPressed : Dictionary = new Dictionary();
		
		public function GameScene() 
		{
			// Add players		
			_playerA = new Player( this );			
			_playerA.x = _offset;
			_playerA.y = Constants.centerY;
						
            _playerB = new Player( this );
			_playerB.x = Constants.gameWidth - (_offset+Player.HALF_PLAYER_WIDTH);
			_playerB.y = Constants.centerY;
           			
			// Add center line
			_setupCenterLine();
			
			// Add ping pong
			_pingPong = new PingPong( this );				
			
			// Add score broad
			_scoreBoard = new ScoreBoard( this );				
		}
		
		public override function init():void 
		{							
			this.stage.addEventListener( KeyboardEvent.KEY_DOWN, _keyDownFunction );
			this.stage.addEventListener( KeyboardEvent.KEY_UP, _keyUpFunction );
			this.stage.addEventListener( Event.ENTER_FRAME, _updateFrameFunction );
		}
				
		private function _updateFrameFunction( e:Event ):void
		{
			if ( !_pingPong.active ) // ping pong reborn
			{				
				_emitThePong();				
				_pingPong.active = true;
			}			
					
			_playerA.update();
			_playerB.update();
			_pingPong.update();
			
			if ( !_computePongMovement() )
			{
				_pingPong.active = false;
			}
		}
		
		private function _computePongMovement():Boolean
		{
			var num:int;
			
			if (_pingPong.minPoint.x <= 0)
			{
				num = _scoreBoard.scoreB;		
				++num;
				_scoreBoard.scoreB = num;
				
				return false; // is dead
			}			
			else if ( _pingPong.maxPoint.x >= Constants.gameWidth ) 
			{
				num = _scoreBoard.scoreA;	
				++num;
				_scoreBoard.scoreA = num;
				
				return false; // is dead
			}						
			else
			{
				_bouncePong();
				return true;
			}				
		}
		
		private function _bouncePong():void
		{
			var dir:Vector2D;
			var tangent:Number;
			var x :Number;
			
			// hit by upper and bottom sides
			if ( (_pingPong.minPoint.y <= 0) || 
				( _pingPong.maxPoint.y >= Constants.gameHeight ) )
			{
				dir = _pingPong.moveDir;
				tangent = dir.y / dir.x;
				tangent = -tangent; // inverse the dir
				x = (dir.x > 0) ? 1: -1;
				var y :Number = tangent * x;
				
				dir.setByValue( x, y );				
				dir.normalize();
				
				if ( _pingPong.minPoint.y <= 0 )
				{
					_pingPong.y = PingPong.HALF_PONG_SIZE;
				}
				else if ( _pingPong.maxPoint.y >= Constants.gameHeight ) 
				{
					_pingPong.y = Constants.gameHeight-PingPong.HALF_PONG_SIZE;
				}
				
				return;
			}						
			
			// hit by playerA
			if ( _playerA.hitThePong( _pingPong ) ) 
			{
				dir = _pingPong.moveDir;
				tangent = dir.y / dir.x;
				x = 1;
				
				dir.setByValue( x, tangent );
				dir.normalize();
			}
			
			// hit by playerB
			if ( _playerB.hitThePong( _pingPong ) ) 
			{
				dir = _pingPong.moveDir;
				tangent = dir.y / dir.x;
				x = -1;
				
				dir.setByValue( x, tangent );
				dir.normalize();
			}
		}
		
		private function _emitThePong():void
		{
			// Reborn positions
			var side:int = (Math.random() > 0.5) ? 1: -1;				
			var yPos:Number = Math.random() // avoid random() is zero
							* (Constants.gameHeight - _offset);
			
			_pingPong.x = Constants.centerX + side * _offset;				
			_pingPong.y = yPos + PingPong.HALF_PONG_SIZE; // vect.y * Constants.centerY + Constants.centerY;
	
			trace( "_pingPong.y: " + _pingPong.y );			
			
			// Reborn direction			
			var moveAngle:Number = Math.tan( Math.random() * Math.PI * _emitAngle );
			var upDir:Number = moveAngle * side;
			
			var dir:Vector2D = new Vector2D( side, upDir );
			dir.normalize();
			_pingPong.moveDir = dir;
			
			trace( "dir.x: " + dir.x  + "dir.y: " + dir.y );			
		}
		
		private function _keyDownFunction( e:KeyboardEvent ):void
		{
			_keyPressed[ e.keyCode ] = true;
			
			trace( "keydown" );
			
			if ( _keyPressed[ KeyCode.W ] ) // W key
			{
				if ( _playerA.minPoint.y > 0 )
				{
					_playerA.y -= _playerA.moveSpeed;
				}				 
			}
			
			if ( _keyPressed[ KeyCode.S ] ) // S key
			{
				if ( (_playerA.maxPoint.y) < Constants.gameHeight )
				{
					_playerA.y += _playerA.moveSpeed;
				}	
			}
			
			if ( _keyPressed[ KeyCode.Up ] ) // Up key
			{
				if ( _playerB.minPoint.y > 0 )
				{
					_playerB.y -= _playerB.moveSpeed;
				}	
			}

			if ( _keyPressed[ KeyCode.Down ] ) // Down key
			{
				if ( _playerB.maxPoint.y < Constants.gameHeight )
				{
					_playerB.y += _playerB.moveSpeed;
				}
			}
		}		
		
		private function _keyUpFunction( e:KeyboardEvent ):void
		{
			trace( "key up" );
			_keyPressed[ e.keyCode ] = false;			
		}		
		
		private function _setupCenterLine():void
		{
			const lineStride:int = 10;
			const lineWidth:int = 10;
			const lineHeight:int = 20;
			const lineNum:int = Math.floor((Constants.gameHeight-lineHeight) / (lineHeight + lineStride));			
			var offset:int = 0;
			
			for ( var i:int = 0; i < lineNum; ++i )
			{
				var lineQuad:Quad = new Quad( lineWidth, lineHeight );
				
				lineQuad.x = Constants.centerX;
				lineQuad.y = offset + (lineStride + lineQuad.height);
				
				offset = lineQuad.y;
				addChild( lineQuad );
			}
			
		}		
		
	}

}